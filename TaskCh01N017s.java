import java.lang.Math;
import java.util.Scanner;
public class TaskCh01N017s {
   public static void main(String[] args) {

       Scanner sc = new Scanner(System.in);
       System.out.println("Enter x:");
       int x = sc.nextInt();   
	   	   
	   double result = Math.abs(x) + Math.abs(x + 1);
	   
	   System.out.println("Result is " + result);

	   
	/*
	Немного доделал программу, чтобы можно было проверить, 
	правильный ли выводится результат через командную строку.
	*/
   }
}