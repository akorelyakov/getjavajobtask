package Tasks;
/*
Написать рекурсивную функцию:
а) вычисления суммы цифр натурального числа;
б) вычисления количества цифр натурального числа.
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        int n = 1123127;

        System.out.println("Сумма цифр числа: " + sumOfDigits(n));
        System.out.println("Количество цифр в числе: " + countOfDigits(n));

    }

    public static int sumOfDigits (int n) {
        if (n<10) return n;
        else {
            return sumOfDigits(n/10) + n%10;

        }

    }

    public static int countOfDigits (int n) {
        if (n<10) return 1;
        else {
            return countOfDigits(n/10) + 1;
        }





    }


}
