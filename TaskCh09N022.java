package Tasks;

import java.util.Scanner;

/*
Дано слово, состоящее из четного числа букв. Вывести на экран его первую половину, не используя оператор цикла.
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        System.out.println("Enter word:");
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.nextLine();

        int halfLength = s.length() / 2;
        char[] result = s.toCharArray();
        for (int i = 0; i < halfLength; i++)
            System.out.print(result[i]);


    }
}
