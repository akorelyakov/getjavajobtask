package Tasks;

import java.util.ArrayList;

public class TaskCh13N012 {
    public static void main(String[] args) {
        Database db1 = new Database();
        db1.addEmployee(new Employee("Альберт", "Иванов", "Петрович", "Омск", 2017, 7));
        db1.addEmployee(new Employee("Сидор", "Сидоров", "Сидорович", "Урюпинск", 2010, 9));
        db1.addEmployee(new Employee("Антон", "Иванов", "Юсуфович", "Москва", 2018, 11));
        db1.addEmployee(new Employee("Антон", "Иванов", "Юсуфович", "Москва", 2011, 11));

        ArrayList<Employee> myLocalDatabase = db1.getDb();

        for (Employee aMyDatabase : myLocalDatabase) {
            if (aMyDatabase.year < 2016 || (aMyDatabase.year == 2016 && aMyDatabase.month < 8)) {
                System.out.println(aMyDatabase.toString());
            }

        }
    }
}

class Database {
    private ArrayList<Employee> db;

    Database() {
        db = new ArrayList<>();
    }

    public void addEmployee(Employee empl){
        db.add(empl);
    }

    public ArrayList<Employee> getDb() {
        return db;
    }
}

class Employee {
    private String name;
    private String patronymic;
    private String surname;
    private String address;
    int year;
    int month;

    Employee(String name, String patronymic, String surname, String address, int year, int month) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.address = address;
        this.year = year;
        this.month = month;
    }

    public String toString() {
        return String.format("Имя: %s, Фамилия: %s, Отчество: %s\nГород: %s, Год: %d, Месяц: %d\n---------------------------",
                name, patronymic, surname, address, year, month);
    }
}