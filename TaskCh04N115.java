package Tasks;

public class TaskCh04N115 {
    public static void main(String[] args) {


        int currentYear = 2018;
        int year = currentYear - 4;
        int animalNumber;
        int colorNumber;

        String animal;
        String color;

        animalNumber = year%12;
        colorNumber = year%5;
        switch (animalNumber) {
            case 0:
                animal = "Крыса";
                break;
            case 1:
                animal = "Корова";
                break;
            case 2:
                animal = "Тигр";
                break;
            case 3:
                animal = "Заяц";
                break;
            case 4:
                animal = "Дракон";
                break;
            case 5:
                animal = "Змея";
                break;
            case 6:
                animal = "Лошадь";
                break;
            case 7:
                animal = "Овца";
                break;
            case 8:
                animal = "Обезьяна";
                break;
            case 9:
                animal = "Петух";
                break;
            case 10:
                animal = "Собака";
                break;
            case 11:
                animal = "Свинья";
                break;
            default: animal = "Invalid";
                break;
        }

        switch (colorNumber) {
            case 0:
                color = "Зеленый";
                break;
            case 1:
                color = "Красный";
                break;
            case 2:
                color = "Желтый";
                break;
            case 3:
                color = "Белый";
                break;
            case 4:
                color = "Черный";
                break;

            default: color = "Invalid";
                break;
        }

        System.out.println(animal + ", " + color);



    }
}
