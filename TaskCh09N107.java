package Tasks;

import java.util.Scanner;

//Дано слово. Поменять местами первую из букв а и последнюю из букв о.
// Учесть возможность того, что таких букв в слове может не быть.

public class TaskCh09N107 {
    public static void main(String[] args) {
        System.out.println("Enter word:");
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.nextLine();
            //позже понял, что гораздо проще сделать с помощью indexOf(int ch), но этот способ тоже работает
        if (!(s.contains("a") && s.contains("o"))) {
            System.out.println("Некорректное слово");

        }
        else {

            int i = 0;
            while (s.charAt(i) != 'a') {
                i++;
            }

            int j = s.length() - 1;
            while (s.charAt(j) != 'o') {
                j--;
            }

            for (int k = 0; k < i; k++) {
                System.out.print(s.charAt(k));
            }
            System.out.print(s.charAt(j));
            for (int k = i + 1; k < j; k++) {
                System.out.print(s.charAt(k));
            }
            System.out.print(s.charAt(i));
            for (int k = j + 1; k < s.length(); k++) {
                System.out.print(s.charAt(k));
            }
        }





    }
}
