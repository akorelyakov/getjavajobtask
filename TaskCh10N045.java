package Tasks;
/*
Даны первый член и разность арифметической прогрессии. Написать рекур-
сивную функцию для нахождения:
а) n-го члена прогрессии;
б) суммы n первых членов прогрессии.
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        double firstNum = 3;
        double difProgres = 12;
        int n = 20;
        System.out.println("N-ый член пргрессии: " + nMember(firstNum, difProgres, n));
        System.out.println("Сумма n первых членов прогрессии: " + sumNMemebers(firstNum, difProgres, n));


    }
    public static double nMember (double firstNum, double difProgress, int n) {
        if (n==1) return firstNum;
        else {
            return nMember(firstNum, difProgress, n-1) + difProgress;
        }

    }

    public static double sumNMemebers (double firstNum, double difProgress, int n) {
        if (n==1) return firstNum;
        else {
            return sumNMemebers(firstNum, difProgress, n-1) + nMember(firstNum, difProgress, n);
        }
    }
}
