package Tasks;

public class TaskCh04N106 {
    public static void main(String[] args) {
        /* Составить программу, которая в зависимости от порядкового номера дня
        месяца (1, 2, ..., 12) выводит на экран время года, к которому относится этот
        месяц.
         */
        int month = 3;
        String period;

        switch (month) {
            case 1:
            case 2:
            case 12:
                period = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                period = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                period = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                period = "Autumn";
                break;
            default: period = "Invalid";
                break;

        }
        System.out.println(period);
    }
}
