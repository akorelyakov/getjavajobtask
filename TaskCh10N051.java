package Tasks;

public class TaskCh10N051 {
    public static void main(String[] args) {
        System.out.println("Первая процедура: " + first(5));
        System.out.println();
        System.out.println("Вторая процедура: " + second(5));
        System.out.println();
        System.out.println("Третья процедура: " + third(5));


    }
    public static int first (int n) {
        if (n>0) {
            System.out.println(n);
            n = first (n-1);
        }
        return n;
    }
    public static int second (int n) {
        if (n>0) {
            n = second (n-1);
            System.out.println(n);
        }
        return n;
    }
    public static int third (int n) {
        if (n>0) {
            System.out.println(n);
            n = third(n-1);
        }
        System.out.println(n);
        return n;
    }
}
