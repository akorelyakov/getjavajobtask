package Tasks;
//Дано слово. Вывести на экран его k-й символ.

import java.io.*;
import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        String a = "автомастерская";
        System.out.println("Номер символа:");
        Scanner sc = new Scanner(System.in);
        int x;
        x = sc.nextInt();

        char aChar = a.charAt(x-1);
        System.out.println(aChar);
    }
}
