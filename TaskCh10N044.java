package Tasks;
/*
Написать рекурсивную функцию нахождения цифрового корня натурального
числа. Цифровой корень данного числа получается следующим образом. Ес-
ли сложить все цифры этого числа, затем все цифры найденной суммы
и повторять этот процесс, то в результате будет получено однозначное число
(цифра), которая и называется цифровым корнем данного числа.
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        int n = 227;
        int m = sumOfDigits(n);
        do {
            m = sumOfDigits(m);
        } while (m>9);
        System.out.println(m);
    }

    public static int sumOfDigits (int n) {
            if (n < 10) return n;
            else {
                return sumOfDigits(n / 10) + n % 10;

            }

    }
}
