package Tasks;

import java.util.Arrays;

public class TaskCh12N023 {
    public static void main(String[] args) {
        array1();

    }
    static void array3() {
        int n = 7;
        int[][] array3 = new int[n][n];
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                if ((j>=i && j<=n-i+1) || ((j<=i) && (j>=n-i+1))) {
                    array3[i][j] = 1;
                }
                else array3[i][j] = 0;
            }
        }
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                System.out.print(array3[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    static void array2() {
        int n=7;
        int[][] array2 = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i==j) || (n-i+1==j)) {
                    array2[i][j] = 1;
                }
                else array2[i][j] = 0;
            }
        }
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                System.out.print(array2[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    static void array1() {
        int n=7;
        int[][] array1 = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i==j)||(i==n-j)) {
                    array1[i][j] = 1;
                }
                else array1[i][j] = 0;
            }
        }
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                System.out.print(array1[i][j]);
            }
            System.out.println();
        }
        System.out.println();


    }
}
