package Tasks;

public class TaskCh12N234 {
    public static void main(String[] args) {
        int n = 3;
        int m = 3;
        int delN = 2;
        int delM = 2;
        int[][] a = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] b = new int[n - 1][m - 1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i != delN && j != delM) {
                    b[i > delN ? i - 1 : i][j > delM ? j - 1 : j] = a[i][j];
                }
            }
        }
    }
}
