package Tasks;

public class TaskCh12N024 {
    public static void main(String[] args) {
        array2();
    }

    static void array1() {
        int n=7;
        int[][] array1 = new int[n][n];
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                if ((i==1)||(j==1)) {
                    array1[i][j] = 1;
                }
                else array1[i][j] = array1[i-1][j]+array1[i][j-1];
            }
        }
        for (int i=1; i<n; i++) {
            for (int j=1; j<n; j++) {
                System.out.print(array1[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
/*
for (int i = 0; i < 6; ++i) {
    for (int j = 0; j < 6; ++j) {
        matrix[i][j] = (i + j) % 6 + 1;
    }
}
for i := 1 to n do
    for j := 1 to n do
      a[i, j] := ((i+j-2) mod n) + 1;
 */
    static void array2() {
        int n=7;
        int[][] array2 = new int[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array2[i][j] = (i+j-2) % 6 + 1;
            }
        }

        for (int i=1; i<n; i++) {
            for (int j=1; j<n; j++) {
                System.out.print(array2[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
