package Tasks.TaskGjjFinal2;

public abstract class Candidates implements Hello { //абстрация - создали абстрактный класс для кандидатов, вынесли в него общие переменные и методы
    private String name;

    public String getName() { //инкапсуляция - закрыли переменные от прямого доступа, сделали сеттеры и геттеры
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void hello(){
        System.out.println("Hi! My name is " + this.getName() + "!");
    }

    public void describeExperience(){};

}
