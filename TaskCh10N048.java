package Tasks;
/*
Написать рекурсивную функцию для вычисления максимального элемента
массива из n элементов.
 */
public class TaskCh10N048 {
    public static void main(String[] args) {
    }
    public class MaxArray {
        int maxIndex(int[] arr, int start) {
            if (isMaxElement(arr, arr[start]))
                return start;
            return maxIndex(arr, start + 1);
        }
        boolean isMaxElement(int[] arr, int element) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] > element)
                    return false;
            }
            return true;
        }
        public int maxIndex(int[] arr) {
            return maxIndex(arr, 0);
        }
    }
}
