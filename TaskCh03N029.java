public class TaskCh03N029 {

    public static void main(String[] args) {
        int x, y, z;
        boolean a;
        x=12;
        y=44;
        z=1;

        /**Записать условие, которое является истинным, когда каждое из чисел X и Y нечетное;
        */
        a = !(x%2==0) & !(y%2==0);
        System.out.println("Первое условие: " + a);

        /** Записать условие, которое является истинным, когда только одно из чисел X и Y меньше или равно 2;
        */
        a = x<=2^y<=2;
        System.out.println("Второе условие: " + a);

        /** Записать условие, которое является истинным, когда хотя бы одно из чисел X и Y равно нулю;
         */
        a = x==0|y==0;
        System.out.println("Третье условие: " + a);

        /** Записать условие, которое является истинным, когда каждое из чисел X, Y, Z отрицательное;
         */
        a = x<0&y<0&z<0;
        System.out.println("Четвертое условие: " + a);

        /** Записать условие, которое является истинным, когда только одно из чисел X, Y и Z меньше или равно 3;
         */
        a = (x<=3&y>3&z>3)|(x>3&y<=3&z>3)|(x>3&y>3&z<=3);
        System.out.println("Пятое условие: " + a);

        /** Записать условие, которое является истинным, когда хотя бы одно из чисел X, Y, Z больше 100
         */
        a = x>100|y>100|z>100;
        System.out.println("Шестое условие: " + a);




    }
}
