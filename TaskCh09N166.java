package Tasks;
//Дано предложение. Поменять местами его первое и последнее слово
//В задачах 9.164—9.183 принять, что:
// в рассматриваемом предложении нет начальных и конечных пробелов и символов "-";
// количество слов в предложении не превышает 10.

import java.util.Scanner;

//Нужно найти индекс первого пробела, найти индекс последнего пробела.
//Далее записать в новый массив первое слово и последнее слово
public class TaskCh09N166 {
    public static void main(String[] args) {
        System.out.println("Enter word:");
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.nextLine();

        int firstSpaceIndex = s.indexOf(' ');
        int lastSpaceIndex = s.lastIndexOf(' ');

        for (int k = lastSpaceIndex + 1; k < s.length(); k++) {
            System.out.print(s.charAt(k));
        }

        for (int k = firstSpaceIndex; k < lastSpaceIndex; k++) {
            System.out.print(s.charAt(k));
        }
        System.out.print(' ');

        for (int k = 0; k < firstSpaceIndex; k++) {
            System.out.print(s.charAt(k));
        }

    }
}
