package Tasks;

public class TaskCh04N015 {
    public static void main(String[] args) {
        int currentYear = 2014;
        int currentMonth = 6;
        int birthYear = 1985;
        int birthMonth = 6;
        int age;

        if (currentMonth >= birthMonth) {
            age = currentYear - birthYear;
        }
        else {
            age = currentYear - birthYear - 1;
        }
        System.out.println(age);
    }

}
