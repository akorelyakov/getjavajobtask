package Tasks;

public class TaskCh10N056 {
    public static void main(String[] args) {
        boolean DigitIsSimple = isSimple(47, 2);
        System.out.println(DigitIsSimple);

    }
    public static boolean isSimple(int n, int i)
    {
        if (n == 1) return false;
        if (n == 2) return true;

        if (n % i == 0) return false;

        if(i <= Math.sqrt(n))
            return isSimple(n, i + 1);
        else return true;
    }
}
