package Tasks;
/*
Написать рекурсивную функцию для вычисления значения так называемой
функции Аккермана для неотрицательных чисел n и m.
Функцию Аккермана называют дважды рекурсивной, т. к. сама функция и
один из ее аргументов определены через самих себя.
Найти значение функции Аккермана для n 1, m 3.
 */
public class TaskCh10N050 {
    public static long akkerman(long n, long m) {
        long a=0;
        if (n==0) a = m+1;
        else if (n!=0&&m==0) {
            a = akkerman(n-1, n);
        }
        else if (n>0&&m>0) {
            a = akkerman(n-1,akkerman(n,m-1));
        }
        return a;
    }
    public static void main(String[] args) {
        long x = akkerman(1,3);
        System.out.println(x);






    }
}
