package Tasks;

import java.util.Scanner;

//9.185.*Строка содержит арифметическое выражение, в котором используются круглые скобки, в том числе вложенные.
// Проверить, правильно ли в нем рас-ставлены скобки.
//а) Ответом должны служить слова да или нет.
//б) В случае неправильности расстановки скобок: если имеются лишние правые (закрывающие) скобки,
// то выдать сооб-щение с указанием позиции первой такой скобки; если имеются лишние левые (открывающие) скобки,
// то выдать сообще-ние с указанием количества таких скобок.
//Если скобки расставлены правильно, то сообщить об этом.
public class TaskCh09N185 {
    public static void main(String[] args) {
        System.out.println("Enter expression:");
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.nextLine();

        int countOpenBracket = 0;
        int countCloseBracket = 0;

        for (int i=0; i<s.length(); i++) {
            if (s.charAt(i) == '(') countOpenBracket += 1;
        }
        for (int i=0; i<s.length(); i++) {
            if (s.charAt(i) == ')') countCloseBracket += 1;
        }
        if (countOpenBracket == countCloseBracket) {
            System.out.println("Correct");
        }
        else {
            if (countOpenBracket >  countCloseBracket) {
                System.out.println(countOpenBracket);

            }
            else {
                System.out.println(s.indexOf(')'));
            }
        }

    }
}
