package Tasks;

public class TaskCh10N049 {
    public static void main(String[] args) {
    }
    public class MaxIndex {
        int maxIndex(int[] arr, int start) {
            if (isMaxElement(arr, arr[start]))
                return start;
            return maxIndex(arr, start + 1);
        }
        boolean isMaxElement(int[] arr, int element) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] > element)
                    return false;
            }
            return true;
        }

    }
}
