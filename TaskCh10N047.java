package Tasks;
/*
Написать рекурсивную функцию для вычисления k-го члена последователь-
ности Фибоначчи. Последовательность Фибоначчи f1, f2, ... образуется по
закону: f1 1; f2 1; fi fi 1 fi 2 ( i 3, 4, ...).
 */
public class TaskCh10N047 {
    public static void main(String[] args) {
        int k = 10;
        System.out.println("К-тый член: " + fibbonachi(k));

    }
    public static int fibbonachi (int k) {
        if (k<2) return 1;
        else {
            int x=0;
            for (int i=0; i<k; i++) {
                x = fibbonachi(k-1) + fibbonachi(k-2);
            }
            return x;

        }
    }
}
