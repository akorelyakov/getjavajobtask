package Tasks;

/*
В некоторых языках программирования (например, в Паскале)
не преду-смотрена операция возведения в степень.
Написать рекурсивную функцию для расчета степени n
вещественного числа a (n — натуральное число).
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        System.out.println(power(3,9.00));
    }

    public static double power (int n, double a) {
        if (n==0)
            return 1;
        return a*power(n-1, a);

    }
}
