import java.lang.Math;
import java.util.Scanner;
public class TaskCh01N017o {
   public static void main(String[] args) {

       Scanner sc = new Scanner(System.in);
       System.out.println("Enter x:");
       int x = sc.nextInt();
	   double result = Math.sqrt(1 - Math.pow(Math.sin(x), 2));
	   System.out.println("Result is " + result);
	/*
	Немного доделал программу, чтобы можно было проверить, 
	правильный ли выводится результат через командную строку.
	*/
   }
}
