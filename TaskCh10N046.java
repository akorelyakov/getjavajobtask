package Tasks;
/*
Даны первый член и знаменатель геометрической прогрессии. Написать ре-
курсивную функцию:
а) нахождения n-го члена прогрессии;
б) нахождения суммы n первых членов прогрессии.
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        double firstMember = 12;
        double denominator = 2;
        int n = 10;
        System.out.println("N-ый член прогрессии: " + nMember(firstMember, denominator, n));
        System.out.println("Сумма первых n членов прогрессии: " + nSum(firstMember, denominator, n));

       
    }
    public static double nMember (double firstMember, double denominator, int n) {
        if (n==1) return firstMember;
        else {
            return nMember(firstMember, denominator, n-1)*denominator;
        }

    }
    public static double nSum (double firstMember, double denominator, int n) {
        if (n==1) return firstMember;
        else {
            return nSum(firstMember, denominator, n-1) + nMember(firstMember, denominator, n);
        }
    }
}
