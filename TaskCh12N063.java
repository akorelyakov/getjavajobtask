package Tasks;

import java.util.Random;

/*
В двумерном массиве хранится информация о количестве учеников
 в том или ином классе каждой параллели школы с первой по
 одиннадцатую (в пер-вой строке — информация о количестве
 учеников в первых классах, во вто-рой — о вторых и т. д.).
  каждой параллели имеются 4 класса. Определить среднее
  количество учеников в классах каждой параллели.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] array = new int[11][4];
        double sum;
        double average;

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                int min = 10; //пусть в каждой параллели от 10 до 100 учеников
                int max = 100;
                int diff = max - min;
                Random random = new Random();
                int q = random.nextInt(diff + 1);
                q += min;

                array[i][j] = q;
            }
        }

        for (int i = 0; i < 11; i++) { //вывести массив на экран
            for (int j = 0; j < 4; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();

        for (int i = 0; i < 11; i++) {
            sum = 0;
            average = 0;
            for (int j = 0; j < 4; j++) {
                sum += array[i][j];
                average = sum/4;
            }
            System.out.println(average);
        }

    }
}
