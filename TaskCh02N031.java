import java.lang.Math;
import java.util.Scanner;
public class TaskCh02N031 {
   public static void main(String[] args) {

       Scanner sc = new Scanner(System.in);
       System.out.println("Enter x, 100<=x<=999:");
       int x = sc.nextInt();   
       System.out.println("First number: " + x);
       double x1 = (double) x;

       double a = 0, b = 0, c = 0;

       a = Math.floor(x1 / 100);
       x1 = x1 - a * 100;
       b = Math.floor(x1 / 10);
       x1 = x1 - b * 10;
       c = x1;
       
       int a1 = (int) a;
       int b1 = (int) b;
       int c1 = (int) c;

       System.out.print("Second number : ");
       System.out.print(a1);
       System.out.print(c1);
       System.out.print(b1); 
      

   }
}