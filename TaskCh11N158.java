package Tasks;


import java.util.Arrays;

public class TaskCh11N158 {
    public static void main(String[] args){
        int[] a = {1,2,3,4,6,6,3,1,2};
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(TaskCh11N158.noRepeat(a)));
    }
    public static int[] noRepeat(int[] a) {
        boolean repeat = true;
        int[] b = new int[a.length];
        for(int i=0, k=0; i<a.length; i++) {
            repeat = true;
            for(int j=i+1; j<a.length-1; j++) {
                if(a[i]==a[j]) repeat = false;
            }
            if(repeat) b[k++]=a[i];
        }
        return b;
    }
}



