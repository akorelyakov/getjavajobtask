package Tasks;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        System.out.println("Enter word:");
        Scanner sc = new Scanner(System.in);
        String s;
        s = sc.nextLine();

        char[] result = s.toCharArray();

        for (int i = s.length()-1; i>=0; i--)
            System.out.print(result[i]);

    }
}
