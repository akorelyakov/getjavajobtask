package Tasks;

public class TaskCh04N067 {
    public static void main(String[] args) {

       /* Дано целое число k (1 <= k <= 365). Определить, каким будет k-й день года: вы-
          ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.
       */
       int k = 7;
       String weekDay;

       if (k%7 == 0 || k%7 == 6) {
           weekDay = "Weekend";
       }
       else {
           weekDay = "Workday";
       }
        System.out.println(weekDay);
    }
}
