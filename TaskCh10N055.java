package Tasks;

public class TaskCh10N055 {
    public static void main(String[] args) {
        System.out.println(getDigit(2));

    }

    public static int N = 2;
    public static String[] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

    public static String getDigit(int number){
        return (number == 0) ? "" : getDigit(number / N) + digits[number % N];
    }



}
